<?php
/**
 * The template for displaying pages
 */

get_header(); ?>

<div class="header-news">
    <div class="parallax-scene" id="scene">
        <div class="header-bg-layer-1" data-depth="0.2"></div>
        <!-- /.header-bg-layer-1 -->
        <div class="header-bg-layer-2" data-depth="0.1"></div>
        <!-- /.header-bg-layer-2 -->
        <div class="header-bg-layer-3" data-depth="0.3"></div>
        <!-- /.header-bg-layer-2 -->
    </div>
    <div class="container">
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
        the_content();
        endwhile;
        ?>
    </div>
    <!-- /.container -->
</div>
<!-- /.news-category -->

<!-- /.callback-section -->

<?php  get_template_part( 'template-parts/callback-section'); ?>

<!-- /.callback-section -->

<?php get_footer(); ?>
