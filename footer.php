<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
</div>
<footer>
    <div class="container">
        <div class="footer-wrap">
            <div>
                <span class="logo">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-dark.svg" alt="image">
                </span>
                <!-- /.logo -->
                <div class="copyright">
                    <?php the_field('copyright', 'option'); ?>
                                   </div>
                <!-- /.copyright -->
            </div>

            <div class="social-link">
                <a href="<?php the_field('copyright', 'option'); ?>" class="icon-fb"></a>
                <a href="<?php the_field('instagram', 'option'); ?>" class="icon-instagram"></a>
                <a href="<?php the_field('twitter', 'option'); ?>" class="icon-twitter"></a>
                <a href="<?php the_field('youtube', 'option'); ?>" class="icon-youtube-symbol"></a>
                <a href="<?php the_field('pinterest', 'option'); ?>" class="icon-pinterest"></a>
                <a href="<?php the_field('google', 'option'); ?>" class="icon-google-plus"></a>
                <a class="email" href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                <!-- /.email -->
            </div>
            <!-- /.social-link -->
        </div>
        <!-- /.footer-wrap -->

    </div>
    <!-- /.container -->
    <a href="https://red-carlos.com" target="_blank" class="rc">
    	by
      <img src="<?php echo get_template_directory_uri(); ?>/assets/img/rc.svg" alt="RedCarlosStudio">
        <style>
					.rc{
						display: block;
						position: absolute;
						bottom: 3px;
						right: 3px;
						width:130px;
						font-size: 11px;
						color: #4d4d4d;
					}
					.rc img{
						width:80%;
						vertical-align: middle;
					}
				</style>
    </a>
</footer>



<script defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5BEFeffM4Rw-Zjz0s-D_BAxbtnIyaU8E&callback">
</script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js"></script>
<?php wp_footer(); ?>
<script>
    $(window).on('load', function () {
        var $preloader = $('.preloader'),
            $spinner   = $preloader.find('.preloader-wrap');
        $spinner.fadeOut();
        $preloader.delay(350).fadeOut('slow');
    });
</script>
</body>
</html>
