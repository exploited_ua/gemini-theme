<?php
/**
 * Template Name: home
 *
 */

get_header();
?>
<?php $page_id = get_the_ID(); ?>
<div class="header-section">
    <div class="parallax-scene" id="scene">
        <div class="header-bg-layer-1" data-depth="0.2"></div>
        <!-- /.header-bg-layer-1 -->
        <div class="header-bg-layer-2" data-depth="0.1"></div>
        <!-- /.header-bg-layer-2 -->
        <div class="header-bg-layer-3" data-depth="0.3"></div>
        <!-- /.header-bg-layer-2 -->
    </div>

    <div class="container">
        <div class="header-section-wrap">
            <?php while ( have_rows('banner_block', $page_id) ) : the_row(); ?>
                <div class="header-title">
                    <?php the_sub_field('top_text_1'); ?>
                </div>
                <!-- /.header-title -->

                <div class="header-image">
                    <?php $image_src = get_sub_field('top_img_1'); ?>
                    <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">

                </div>
            <?php endwhile; ?>
            <!-- /.header-image -->
        </div>
        <!-- /.header-section-wrap -->
        <div class="scroll-down">
            <p><?php the_field('arrow_text', $page_id); ?></p>
            <div class="scroll-btn">
                <div class="scroll-btn-body"></div>
                <div class="scroll-btn-arrow"></div>
                <!-- /.scroll-btn-body -->
            </div>
            <!-- /.scroll-btn -->
        </div>
        <!-- /.scroll-down -->
    </div>
    <!-- /.container -->
</div>
<!-- /.header-section -->

<?php  get_template_part( 'template-parts/advantages-section'); ?>

<!-- /.advantage_section -->
<div class="shop-section">
    <div class="container">
        <div class="shop-block">
            <?php
            /*
             * Get all product_category
             */
            $terms = get_terms( array(
                'taxonomy' => 'product-category',
                'parent' => 0,
                'order' => 'DESC',
                'include'       => array('6', '56', '7')
            ) );?>
            <?php foreach($terms as $term): ?>
                <a href="<?php echo get_term_link($term->term_id); ?>" class="shop-item tea-item">
                    <?php $image_src = get_field('cat_img', $term ); ?>
                    <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                    <strong>
                        <?php echo $term->name; ?>
                    </strong>
                    <p>
                        <?php  echo $term->description; ?>
                    </p>
                </a>
            <?php endforeach; ?>
            <!-- /.shop-item -->

            <!-- /.shop-item -->
            <a href="<?php the_field('href_online_shop','option'); ?>" class="shop-link">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/shop_img.png"
                     srcset="<?php echo get_template_directory_uri(); ?>/assets/img/shop_img.png 1x, <?php echo get_template_directory_uri(); ?>/assets/img/shop_img@2x.png 2x, <?php echo get_template_directory_uri(); ?>/assets/img/shop_img@3x.png 3x" alt="image">
                <strong>
                    <?php the_field('online_shop', 'option'); ?>
                </strong>
            </a>
            <!-- /.shop-item -->
        </div>
        <!-- /.shop-block -->
    </div>
    <!-- /.container -->
</div>
<!-- /.shop-section -->

<div class="equipment-section">
    <div class="container">
        <div class="equipment-block">
            <?php while ( have_rows('equipments') ) : the_row(); ?>
                <div class="equipment-info">
                    <?php the_sub_field('equipment_desc'); ?>
                    <a href="<?php the_sub_field('button_href_1'); ?>" class="btn"><?php the_sub_field('button_text_1'); ?></a>
                </div>
                <!-- /.equipment-info -->
                <div class="equipment-image">
                    <div class="equipment-image-wrap">
                        <?php $image_src = get_sub_field('equipment_img'); ?>
                        <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                    </div>
                    <!-- /.equipment-image-wrap -->

                    <div class="equipment-image-shadow"></div>
                    <!-- /.equipment-image-shadow -->
                </div>
                <!-- /.equipment-image -->
            <?php endwhile; ?>
        </div>
        <!-- /.equipment-block -->
    </div>
    <!-- /.container -->
</div>
<!-- /.equipment-section -->


<div class="app-section" style="display: none;">
    <div class="container">
        <div class="app-section-wrap">
            <?php while ( have_rows('gemini_club_left_block') ) : the_row(); ?>
                <div class="apple-block">
                    <div class="img-wrap">
                        <?php $image_src = get_sub_field('img'); ?>
                        <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                    </div>
                    <!-- /.img-wrap -->
                    <a href="<?php the_sub_field('href'); ?>">
                        <?php $image_src = get_sub_field('img_href'); ?>
                        <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                    </a>
                </div>
            <?php endwhile; ?>
            <!-- /.apple-block -->
            <?php while ( have_rows('gemini_club_middle_block') ) : the_row(); ?>
                <div class="app-block">
                    <?php the_sub_field('desc_gemini_club'); ?>
                    <div class="app-calback">
                        <ul>
                            <?php $i = 1; while ( have_rows('gemini_advantages') ) : the_row(); ?>
                                <li>
                                    <strong><?php echo $i; ?></strong> <?php the_sub_field('adv_desc'); ?>
                                </li>
                                <?php $i++; endwhile; ?>
                        </ul>
                        <form class="subscribe-form">
                            <strong><?php the_sub_field('form_title'); ?></strong>
                            <input id="contact_phone" required type="tel" placeholder="<?php the_sub_field('phone_placeholder'); ?>">
                            <button type="submit" class="btn"><?php the_sub_field('phone_submit_button'); ?></button>
                        </form>
                    </div>
                    <!-- /.app-calback -->
                </div>
            <?php endwhile; ?>
            <!-- /.app-form-block -->

            <?php while ( have_rows('gemini_club_right_block') ) : the_row(); ?>

                <div class="android-block">
                    <div class="img-wrap">
                        <?php $image_src = get_sub_field('img'); ?>
                        <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                    </div>
                    <!-- /.img-wrap -->
                    <a href="<?php the_sub_field('href'); ?>">
                        <?php $image_src = get_sub_field('img_href'); ?>
                        <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                    </a>
                </div>
            <?php endwhile; ?>

        </div>
        <!-- /.app-section-wrap -->
    </div>


    <!-- /.container -->
</div>
<!-- /.app-section -->

<div class="news-section">
    <div class="container">
        <h2><?php the_field('title_block_news', $page_id); ?></h2>

        <div class="news-block">
            <?php
            $args = array(
                'order' => 'DESC',
                'posts_per_page' => 4
            );
            $the_query = new WP_Query($args);
            while ( $the_query->have_posts()) :
                $the_query->the_post(); ?>
                <div class="news-item">
                    <a href="<?php the_permalink(); ?>" class="news-img-wrap">
                    <?php the_post_thumbnail(); ?>
                    </a>
                    <!-- /.news-img-wrap -->
                    <div class="news-text-wrap">
                        <h3><?php the_title(); ?></h3>
                    <span class="public-date">
                        <?php echo get_the_date(); ?>
                    </span>
                        <!-- /.public-date -->
                        <p>
                            <?php
                            $content = get_the_content();
                            echo wp_trim_words( $content , 40, '...' );
                            ?>
                            <?php global $sitepress;
                            $current_language = $sitepress->get_current_language();?>
                            <a href="<?php the_permalink(); ?>"><?php echo __('MORE','gemini'); ?>
                            </a>
                        </p>
                    </div>
                    <!-- /.news-text-wrap -->
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
        <!-- /.news-container -->
    </div>
    <!-- /.container -->
</div>
<!-- /.news-section -->

<!-- /.callback-section -->

        <?php  get_template_part( 'template-parts/callback-section'); ?>

<!-- /.callback-section -->


<?php get_footer(); ?>
