<?php
/**
 * The template part for displaying category-list content
 */
?><div class="shop-section">
    <div class="container">
        <div class="shop-block">
            <?php
            /*
             * Get all product_category
             */
            
            $terms = get_terms( array(
                'taxonomy' => 'product-category',
                'parent' => 0,
                'exclude' => array($cur_term->term_id),
                'order' => 'DESC'
            ) );?>
            <?php foreach($terms as $term): ?>
                <a href="<?php echo get_term_link($term->term_id); ?>" class="shop-item tea-item">
                    <?php $image_src = get_field('cat_img', $term ); ?>
                    <img src="<?php echo $image_src; ?>" srcset="<?php echo $image_src; ?> , <?php echo get_srcset_by_img_src($image_src); ?>" alt="image">
                    <strong>
                        <?php echo $term->name; ?>
                    </strong>
                    <p>
                        <?php  echo $term->description; ?>
                    </p>
                </a>
            <?php endforeach; ?>
            <!-- /.shop-item -->

            <!-- /.shop-item -->
            <a href="<?php the_field('href_online_shop','option'); ?>" class="shop-link">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/shop_img.png"
                     srcset="<?php echo get_template_directory_uri(); ?>/assets/img/shop_img.png 1x, <?php echo get_template_directory_uri(); ?>/assets/img/shop_img@2x.png 2x, <?php echo get_template_directory_uri(); ?>/assets/img/shop_img@3x.png 3x" alt="image">
                <strong>
                    <?php the_field('online_shop', 'option'); ?>
                </strong>

            </a>
            <!-- /.shop-item -->
        </div>
        <!-- /.shop-block -->
    </div>
    <!-- /.container -->
</div>
<!-- /.shop-section -->