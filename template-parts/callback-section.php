<?php
/**
 * The template part for displaying callback-section content
 */
?>
<div class="callback-section">
    <div class="container">

        <div class="callback-block">
            <div class="map" id="map"></div>
            <script>
                var img_src = '<?php echo get_template_directory_uri(); ?>/assets/img/map_icon.png'
            </script>
            <!-- /.map -->
            <?php while ( have_rows('dealers', 'option') ) : the_row(); ?>

                <div class="contact-info">
                    <ul>
                        <li class="icon-map">
                            <strong><?php the_sub_field('title_adress'); ?></strong>
                            <p><?php the_sub_field('adress'); ?></p>
                        </li>
                        <li class="icon-phone">
                            <strong><?php the_sub_field('title_phone'); ?></strong>
                            <?php the_sub_field('phones'); ?>
                        </li>
                        <li class="icon-email">
                            <strong><?php the_sub_field('title_mail'); ?></strong>
                            <p><?php the_sub_field('mail'); ?></p>
                        </li>
                        <li class="icon-time">
                            <strong><?php the_sub_field('title_work_time'); ?></strong>
                            <p><?php the_sub_field('work_time'); ?></p>
                        </li>
                    </ul>
                </div>

            <?php endwhile; ?>
            <!-- /.dealer-item -->
            <?php while ( have_rows('contact_form', 'option') ) : the_row(); ?>
                <form class="calback-form">
                    <h2><?php the_sub_field('contact_form_title'); ?></h2>
                    <input required id="contact_name" type="text" placeholder="<?php the_sub_field('name_placeholder'); ?>">
                    <input required id="contact_email" type="email" placeholder="<?php the_sub_field('email_placeholder'); ?>">
                    <textarea required id="contact_msg" placeholder="<?php the_sub_field('msg_placeholder'); ?>"></textarea>
                    <button type="submit" class="btn"><?php the_sub_field('button_title_2'); ?></button>
                </form>
            <?php endwhile; ?>
            <!-- /.calback-form -->
        </div>
        <!-- /.callback-block -->

    </div>
    <!-- /.container -->
</div>
<!-- /.callback-section -->
