<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<?php
// Start the loop.
while (have_posts()) : the_post();
$product_id = get_the_ID();
?>

<div class="header-product" style="background-image: url(<?php the_field('product_banner', $product_id ); ?>)">
    <div class="container">
        <a href="#" onclick="window.location.href = history.back();" class="left-arrow">
            <?php echo __('Back','gemini'); ?>
        </a>
        <a href="<?php the_field('href_online_shop', 'option') ?>" class="btn">
            <?php echo __('In shop','gemini');?>
        </a>
    </div>
    <!-- /.container -->
</div>
<!-- /.header-subcategory -->
<div class="product-section">
    <div class="container">
        <div class="product-name-wrap">
            <div class="main-product-image">
                <div class="product-image-wrap">
                    <?php the_post_thumbnail(); ?>
                </div>
                <!-- /.product-image-wrap -->
            </div>
            <!-- /.main-product-image -->
            <div class="product-name">
                <p><?php the_field('product_sub_title', $product_id ); ?></p>
                <h1>
                    <?php the_title(); ?>
                </h1>
            </div>
            <!-- /.product-name -->
        </div>
        <!-- /.product-name-wrap -->
        <div class="product-info-block">
          <?php the_content(); ?>
        </div>
        <!-- /.product-info -->
    </div>
    <!-- /.container -->
</div>
<!-- /.product-info-section -->
<div class="products-section">
    <div class="container">
        <div class="products-block">
            <?php while( have_rows('related_products', $product_id) ): the_row();
                $post_object = get_sub_field('product');
                $post = $post_object;
                setup_postdata( $post );
                ?>
            <a href="<?php the_permalink(); ?>" class="product-item">
                <div class="product-item-img">
                <?php the_post_thumbnail(); ?>
                </div>
                <!-- /.product-item-img -->
                <p><?php the_title(); ?></p>
            </a>
           <?php endwhile; ?>
            <!-- /.product-item -->

        </div>
        <!-- /.subcategories-product-block -->
    </div>
    <!-- /.container -->
</div>
<!-- /.subcategories-product-section -->


    <!-- /.callback-section -->

    <?php  get_template_part('template-parts/callback-section'); ?>

    <!-- /.callback-section -->

<?php endwhile; ?>
<!-- /.callback-section -->
<?php get_footer(); ?>
