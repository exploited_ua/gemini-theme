$(document).ready(function () {
    $('.search').on('submit', function (e) {
        event.preventDefault();
        var data = {
            action: 'my_search_form',
            security: myajax.nonce,
            beforeSend: function () {
                var $preloader = $('.preloader'),
                    $spinner   = $preloader.find('.preloader-wrap');
                $spinner.fadeIn();
                $preloader.fadeIn();
                $preloader.delay(350).fadeOut('slow');

            },
            cat_id: $('.search-select-input option:selected').val(),
            key: $('.search-input').val()
        };

        jQuery.post(myajax.url, data, function (response) {
            $('body .swiper-scroll-container .swiper-slide-active').html('');
            $('body .swiper-scroll-container .swiper-slide-active').append(response)
        });

    })


    $('.search-select-input').on('change', function (e) {
        event.preventDefault();
        var data = {
            action: 'my_search_category_form',
            security: myajax.nonce,
            beforeSend: function () {
                var $preloader = $('.preloader'),
                $spinner   = $preloader.find('.preloader-wrap');
                $spinner.fadeIn();
                $preloader.fadeIn();
                $preloader.delay(350).fadeOut('slow');

            },
            cat_id: $('.search-select-input option:selected').val()
        };
        jQuery.post(myajax.url, data, function (response) {
            $('body .swiper-scroll-container .swiper-slide-active').html('');
            $('body .swiper-scroll-container .swiper-slide-active').append(response)
        });

    })

});